import { DemoRoutingPage } from './app.po';

describe('demo-routing App', () => {
  let page: DemoRoutingPage;

  beforeEach(() => {
    page = new DemoRoutingPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
