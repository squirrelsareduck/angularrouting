import { Component, OnInit } from '@angular/core';
import {ListService} from "../list.service";

@Component({
  selector: 'app-a',
  templateUrl: './a.component.html',
  styleUrls: ['./a.component.css']
})
export class AComponent implements OnInit {
  items=[];
  constructor(private listService: ListService) {
  }
  navigateAccordingly(e){
    console.log("SQUIRREL " + JSON.stringify(e.target));

  }
  ngOnInit() {
    this.items=this.listService.contentOI;
  }

}
