import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {RouterModule, Routes} from "@angular/router";
import { AComponent } from './a/a.component';
import { BComponent } from './b/b.component';
import {CommonModule} from "@angular/common";
import { ListitemComponent } from './listitem/listitem.component';
import {ListService} from "./list.service";
import { EditComponent } from './edit/edit.component';

const routes: Routes = [
  { path: 'stuff', component: AComponent, children:[
    { path: ':index', component: BComponent},
    { path: ':index/edit', component: EditComponent, outlet:'editsection'}
  ]}
];


@NgModule({
  declarations: [
    AppComponent,
    AComponent,
    BComponent,
    ListitemComponent,
    EditComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule,
    BrowserModule
  ],
  providers: [ListService],
  bootstrap: [AppComponent]
})
export class AppModule { }
