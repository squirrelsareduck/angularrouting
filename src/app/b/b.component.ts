import {Component, OnDestroy, OnInit} from '@angular/core';
import {ListService} from "../list.service";
import {Subscription} from "rxjs/Subscription";
import {ActivatedRoute, Params} from "@angular/router";

@Component({
  selector: 'app-b',
  templateUrl: './b.component.html',
  styleUrls: ['./b.component.css']
})
export class BComponent implements OnInit,OnDestroy {

  contentOI;
  index;
  routeParamsSubscription:Subscription;
  constructor(private listService: ListService,
              private route:ActivatedRoute) { }

  ngOnInit() {
    this.contentOI= this.listService.contentOI;
    this.routeParamsSubscription = this.route.params
      .subscribe(
        (params: Params) => {
          this.index = params['index'];
        }
      );
  }
  ngOnDestroy(){
    this.routeParamsSubscription.unsubscribe();
  }
}
